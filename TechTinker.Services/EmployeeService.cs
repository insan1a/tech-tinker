﻿using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Domain.Dto.Request;
using TechTinker.Domain.Entities;
using TechTinker.Domain.Exceptions;
using TechTinker.Services.Interfaces;

namespace TechTinker.Services;

public class EmployeeService : IEmployeeService
{
    private readonly IEmployeeRepository _employeeRepository;

    public EmployeeService(IEmployeeRepository employeeRepository)
    {
        _employeeRepository = employeeRepository;
    }

    public async Task<Employee> CreateAsync(CreateEmployeeRequest request)
    {
        var model = new Employee(request.FirstName, request.LastName, request.Email, request.Password, request.Role);
        if (await _employeeRepository.AddAsync(model) == false)
            throw new ArgumentException("Employee already exists.");

        return model;
    }

    public async Task<Employee> GetByIdAsync(string employeeId)
    {
        var employee = await _employeeRepository.GetByIdAsync(employeeId);
        if (employee == null)
            throw new EmployeeNotFoundException();

        return employee;
    }

    public async Task<Employee> GetByEmailAsync(string email)
    {
        var employee = await _employeeRepository.GetByEmailAsync(email);
        if (employee == null)
            throw new EmployeeNotFoundException();

        return employee;
    }

    public async Task<IEnumerable<Employee>> GetListAsync()
    {
        var employees = await _employeeRepository.GetAllAsync();
        if (employees == null)
            throw new EmployeeNotFoundException();
        
        return employees;
    }

    public async Task<bool> DeleteAsync(string employeeId)
    {
        if (await _employeeRepository.DeleteAsync(employeeId) == false)
            throw new EmployeeNotFoundException();

        return true;
    }

    public async Task<Employee> UpdateAsync(string id, UpdateEmployeeRequest request)
    {
        var model = new Employee(id, request);
        if (await _employeeRepository.UpdateAsync(model) == false)
            throw new EmployeeNotFoundException();

        return model;
    }
}