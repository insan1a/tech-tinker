﻿using TechTinker.Domain.Entities;

namespace TechTinker.Services.Interfaces;

public interface IConfigurationService
{
    public Task<Configuration> GetByIdAsync(string id);
    public Task<Configuration> CreateAsync(string orderId, string employeeId, IEnumerable<string> componentIds);
    public Task<Configuration> UpdateAsync(string id, IEnumerable<string> componentIds);
    public Task<bool> DeleteAsync(string id);
}