﻿using TechTinker.Domain.Dto.Request;
using TechTinker.Domain.Entities;

namespace TechTinker.Services.Interfaces;

public interface IEmployeeService
{
    public Task<Employee> CreateAsync(CreateEmployeeRequest request);
    public Task<Employee> GetByIdAsync(string employeeId);
    public Task<Employee> GetByEmailAsync(string email);
    public Task<IEnumerable<Employee>> GetListAsync();
    public Task<bool> DeleteAsync(string employeeId);
    public Task<Employee> UpdateAsync(string id, UpdateEmployeeRequest request);
}