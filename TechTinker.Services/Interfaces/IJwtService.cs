﻿using System.Security.Claims;
using TechTinker.Domain.Entities;

namespace TechTinker.Services.Interfaces;

public interface IJwtService
{
    public string GenerateToken(Claims claims);
    public ClaimsPrincipal ValidateToken(string token);
}
