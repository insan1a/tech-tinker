﻿using TechTinker.Domain.Entities;

namespace TechTinker.Services.Interfaces;

public interface IStatisticService
{
    public Task<Statistic> GetForEmployeeAsync(DateTime from, DateTime to, string id);
    public Task<IEnumerable<Statistic>> GetForEmployeesAsync(DateTime from, DateTime to, IEnumerable<string> ids);
}