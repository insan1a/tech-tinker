﻿using TechTinker.Domain.Entities;

namespace TechTinker.Services.Interfaces;

public interface IOrderService
{
    public Task<IEnumerable<Order>> GetOrdersByEmployeeIdAsync(string id);
    public Task<Order> GetOrderByIdAsync(string id);
}