﻿using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Domain.Entities;
using TechTinker.Services.Interfaces;

namespace TechTinker.Services;

public class ConfigurationService : IConfigurationService
{
    private readonly IOrderRepository _orderRepository;
    private readonly IConfigurationRepository _configurationRepository;
    private readonly IComponentRepository _componentRepository;

    public ConfigurationService(IConfigurationRepository configurationRepository, IOrderRepository orderRepository, 
        IComponentRepository componentRepository)
    {
        _configurationRepository = configurationRepository;
        _orderRepository = orderRepository;
        _componentRepository = componentRepository;
    }

    public async Task<Configuration> GetByIdAsync(string id)
    {
        var configuration = await _configurationRepository.GetByIdAsync(id);
        return configuration;
    }

    public async Task<Configuration> CreateAsync(string orderId, string employeeId, IEnumerable<string> componentIds)
    {
        if (componentIds.Any() == false)
            throw new ArgumentException("Components must not be empty");
        
        if (componentIds.Count() > 10)
            throw new ArgumentException("Components must not be more than 10");

        if (componentIds.GroupBy(x => x).Any(g => g.Count() > 1))
            throw new ArgumentException("Components must not have the same id");
        
        var components = await _componentRepository.GetByIdsAsync(componentIds);
        
        if (components.Count() != componentIds.Count())
            throw new ArgumentException("Some components are not found");

        if (components.GroupBy(x => x.Type).Any(g => g.Count() > 1))
            throw new ArgumentException("Components must not have the same type");

        if (components.Any(x => x.ConfigurationId != null))
            throw new ArgumentException("Components must not be configured already");
        
        var order = await _orderRepository.GetByIdAsync(orderId);

        var total = components.Sum(x => x.Price);

        if (order.PriceLimit < total)
            throw new ArgumentException("Price limit is less than the configuration price");

        var configuration = new Configuration()
        {
            OrderId = orderId,
            EmployeeId = employeeId,
            Components = components.ToList(),
            Price = total,
        };
        
        configuration = await _configurationRepository.AddToOrderAsync(configuration);

        return configuration;
    }

    public async Task<Configuration> UpdateAsync(string id, IEnumerable<string> componentIds)
    {
        if (componentIds.Any() == false)
            throw new ArgumentException("Components must not be empty");
        
        if (componentIds.Count() > 10)
            throw new ArgumentException("Components must not be more than 10");

        if (componentIds.GroupBy(x => x).Any(g => g.Count() > 1))
            throw new ArgumentException("Components must not have the same id");
        
        
        var components = await _componentRepository.GetByIdsAsync(componentIds);
        
        if (components.Count() != componentIds.Count())
            throw new ArgumentException("Some components are not found");

        if (components.GroupBy(x => x.Type).Any(g => g.Count() > 1))
            throw new ArgumentException("Components must not have the same type");

        if (components.Any(x => x.ConfigurationId != null && x.ConfigurationId != id))
            throw new ArgumentException("Components must not be configured already");

        var configuration = await _configurationRepository.GetByIdAsync(id);
        if (configuration == null)
            throw new ArgumentException("Configuration is not found");

        var order = await _orderRepository.GetByIdAsync(configuration.OrderId);
        var total = components.Sum(x => x.Price);

        if (order.PriceLimit < total)
            throw new ArgumentException("Price limit is less than the configuration price");

        configuration.Components = components.ToList();
        configuration.Price = total;

        configuration = await _configurationRepository.UpdateAsync(configuration);

        return configuration;
    }

    public Task<bool> DeleteAsync(string id)
    { 
        return _configurationRepository.DeleteAsync(id);
    }
}