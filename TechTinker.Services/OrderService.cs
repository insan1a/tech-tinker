﻿using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Domain.Entities;
using TechTinker.Services.Interfaces;

namespace TechTinker.Services;

public class OrderService : IOrderService
{
    private readonly IEmployeeRepository _employeeRepository;

    public OrderService(IEmployeeRepository employeeRepository)
    {
        _employeeRepository = employeeRepository;
    }

    public async Task<IEnumerable<Order>> GetOrdersByEmployeeIdAsync(string id)
    {
        var orders = await _employeeRepository.GetOrdersAsync(id);
        return orders;
    }

    public async Task<Order> GetOrderByIdAsync(string id)
    {
        var order = await _employeeRepository.GetOrderAsync(id);
        return order;
    }
}