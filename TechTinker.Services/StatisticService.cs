﻿using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Domain.Dto.Response;
using TechTinker.Domain.Entities;
using TechTinker.Services.Interfaces;

namespace TechTinker.Services;

public class StatisticService : IStatisticService
{
    private readonly IEmployeeRepository _employeeRepository;
    private readonly IConfigurationRepository _configurationRepository;

    public StatisticService(IEmployeeRepository employeeRepository, IConfigurationRepository configurationRepository)
    {
        _employeeRepository = employeeRepository;
        _configurationRepository = configurationRepository;
    }

    public async Task<Statistic> GetForEmployeeAsync(DateTime from, DateTime to, string id)
    {
        if (from.Equals(DateTime.MinValue) || to.Equals(DateTime.MinValue))
            throw new ArgumentException($"Invalid date {from} or {to}");
        
        if (from > to)
            throw new ArgumentException($"From date {from} is greater than to date {to}");
        
        if (to.Subtract(from).Days  > 31)
            throw new ArgumentException($"Period is greater than 31 days");
        
        var orders = await _employeeRepository.GetOrdersAsync(id);
        if (orders == null)
            throw new ArgumentException($"Orders not found for employee {id}");

        orders = orders.Where(x => x.UpdatedAt >= from && x.UpdatedAt <= to && x.Status == OrderStatus.Completed);

        var budgets = new Dictionary<BudgetType, int>
        {
            {BudgetType.LowerThan50K, 0},
            {BudgetType.Between50KAnd100K, 0}, 
            {BudgetType.Between100KAnd200K, 0},
            {BudgetType.HigherThan200K, 0}
        };

        foreach (var order in orders)
        {
            var priceLimit = order.PriceLimit;

            switch (priceLimit)
            {
                case < 50000:
                    budgets[BudgetType.LowerThan50K]++;
                    break;
                case < 100000:
                    budgets[BudgetType.Between50KAnd100K]++;
                    break;
                case < 200000:
                    budgets[BudgetType.Between100KAnd200K]++;
                    break;
                default:
                    budgets[BudgetType.HigherThan200K]++;
                    break;
            }
        }
        
        // if some budget count is 0, than remove it
        budgets = budgets.Where(x => x.Value > 0).
            ToDictionary(x => x.Key, x => x.Value);
        
        // for each budget create a list of Budget object
        var budgetsList = budgets.Select(x => new Budget(x.Key, x.Value)).ToList();
        
        var orderIds = orders.Select(x => x.Id).ToList();
        var configurations = await _configurationRepository.GetForOrdersAsync(orderIds);
        var total = configurations.Sum(x => x.Price);
        
        // create Statistic object with Period, Total (sum of configuration prices of orders), budgets
        var statistic = new Statistic()
        {
            Period = new Period(from, to),
            Total = total,
            Budgets = budgetsList,
        };

        return statistic;
    }

    public async Task<IEnumerable<Statistic>> GetForEmployeesAsync(DateTime from, DateTime to, IEnumerable<string> ids)
    {
        var statistics = new List<Statistic>();

        foreach (var id in ids)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            var statistic = await GetForEmployeeAsync(from, to, id);
            statistic.Employee = new EmployeeResponse(employee);
            statistics.Add(statistic);
        }

        return statistics;
    }
}