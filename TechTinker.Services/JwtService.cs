﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using TechTinker.Domain.Entities;
using TechTinker.Services.Interfaces;

namespace TechTinker.Services;

public class JwtService : IJwtService
{
    private readonly string _secretKey = Environment.GetEnvironmentVariable("JWT_SECRET_KEY") ?? "4ffc85e0-7935-4e1a-810b-1a89b3d409af";
    private readonly string _issuer = Environment.GetEnvironmentVariable("JWT_ISSUER") ?? "techtinker";
    private readonly string _audience = Environment.GetEnvironmentVariable("JWT_AUDIENCE") ?? "techtinker";
    
    public string GenerateToken(Claims claims)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = GetSigningKey();
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
               new Claim(ClaimTypes.NameIdentifier, claims.EmployeeId),
               new Claim(ClaimTypes.Role, claims.Role.ToString())
            }),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256),
            Issuer = _issuer,
            Audience = _audience,
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    public ClaimsPrincipal ValidateToken(string token)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = GetSigningKey();
        var validationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidIssuer = _issuer,
            ValidateAudience = true,
            ValidAudience = _audience,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            RequireExpirationTime = true,
            ValidateLifetime = true
        };
        return tokenHandler.ValidateToken(token, validationParameters, out _);
    }
    
    private byte[] GetSigningKey()
    {
        var hash = SHA256.HashData(Encoding.ASCII.GetBytes(_secretKey));
        return hash.ToArray();
    }
}

