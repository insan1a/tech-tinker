﻿using Microsoft.AspNetCore.Mvc;
using TechTinker.Domain.Dto.Request;
using TechTinker.Domain.Dto.Response;
using TechTinker.Domain.Entities;
using TechTinker.Domain.Exceptions;
using TechTinker.Services.Interfaces;

namespace TechTinker.Api.Controllers;

[ApiController]
[Route("/api/auth")]
public class AuthController : Controller
{
    private readonly  ILogger<AuthController> _logger;
    private readonly IJwtService _jwtService;
    private readonly IEmployeeService _employeeService;

    public AuthController(ILogger<AuthController> logger, IJwtService jwtService, IEmployeeService employeeService)
    {
        _logger = logger;
        _jwtService = jwtService;
        _employeeService = employeeService;
    }

    [HttpPost]
    [Route("token")]
    public async Task<IActionResult> Token(Credentials credentials)
    {
        try
        {
            var employee = await _employeeService.GetByEmailAsync(credentials.Email);
            if (employee.VerifyPassword(credentials.Password) == false)
            {
                _logger.LogInformation("Invalid credentials");
                return Unauthorized(new ErrorResponse("invalid credentials"));
            }

            var token = _jwtService.GenerateToken(new Claims(employee.Id, employee.Role));

            _logger.LogInformation("Token generated for employee with id {id}", employee.Id);

            return Ok(new AccessTokenResponse(token));
        }
        catch (EmployeeNotFoundException ex)
        {
            _logger.LogError(ex, "Invalid credentials");

            return Unauthorized(new ErrorResponse("invalid credentials"));
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Internal server error");

            return StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse("internal server error"));
        }
    }
}