﻿using Microsoft.AspNetCore.Mvc;
using TechTinker.Domain.Dto.Request;
using TechTinker.Domain.Dto.Response;
using TechTinker.Domain.Entities;
using TechTinker.Services.Interfaces;

namespace TechTinker.Api.Controllers;

[ApiController]
[Route("/api/configs")]
public class ConfigurationController : Controller
{
    private readonly ILogger<ConfigurationController> _logger;
    private readonly IConfigurationService _configurationService;

    public ConfigurationController(ILogger<ConfigurationController> logger, IConfigurationService configurationService)
    {
        _logger = logger;
        _configurationService = configurationService;
    }

    [HttpGet]
    [Route("{id}")]
    public async Task<IActionResult> GetConfiguration(string id)
    {
        try
        {
            var configuration = await _configurationService.GetByIdAsync(id);

            var response = new ConfigurationResponse(configuration)
            {
                Components = configuration.Components.Select(c => new ComponentResponse(c)).ToList(),
            };

            return Ok(response);
        }
        catch (ArgumentException ex)
        {
            _logger.LogError(ex, "Error getting configuration");
            return BadRequest(new ErrorResponse(ex.Message));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error getting configuration");

            return StatusCode(StatusCodes.Status500InternalServerError);
        }        
    }

    [HttpPost]
    public async Task<IActionResult> CreateConfiguration([FromBody] CreateConfigurationRequest body)
    {
        try
        {
            if (HttpContext.Items["Employee"] is not Employee employee)
                return StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse("internal server error"));
            
            var configuration = await _configurationService.CreateAsync(body.OrderId, employee.Id, body.ComponentIds);

            return Ok(configuration);
        }
        catch (ArgumentException ex)
        {
            _logger.LogError(ex, "Error creating configuration");

            return BadRequest(new ErrorResponse(ex.Message));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error creating configuration");

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    [HttpPut]
    [Route("{id}")]
    public async Task<IActionResult> UpdateConfiguration(string id, [FromBody] UpdateConfigurationRequest body)
    {
        try
        {
            var configuration = await _configurationService.UpdateAsync(id, body.ComponentIds);
            return Ok(configuration);
        }
        catch (ArgumentException ex)
        {
            _logger.LogError(ex, "Error updating configuration");

            return BadRequest(new ErrorResponse(ex.Message));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error updating configuration");

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    [HttpDelete]
    [Route("{id}")]
    public async Task<IActionResult> DeleteConfiguration(string id)
    {
        try
        {
            if (await _configurationService.DeleteAsync(id) == false)
                return NotFound(new { id });

            return Ok(new { id });
        }
        catch (ArgumentException e)
        {
            _logger.LogError(e, "Error deleting configuration");
            
            return BadRequest(new ErrorResponse(e.Message));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error deleting configuration");

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}