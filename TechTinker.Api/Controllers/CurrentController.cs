﻿using Microsoft.AspNetCore.Mvc;
using TechTinker.Domain.Dto.Response;
using TechTinker.Domain.Entities;
using TechTinker.Services.Interfaces;

namespace TechTinker.Api.Controllers;

[ApiController]
[Route("/api/current")]
public class CurrentController : Controller
{
    private readonly ILogger<CurrentController> _logger;
    private readonly IEmployeeService _employeeService;
    private readonly IOrderService _orderService;
    private readonly IStatisticService _statisticService;

    public CurrentController(ILogger<CurrentController> logger, IEmployeeService employeeService,
        IOrderService orderService, IStatisticService statisticService)
    {
        _logger = logger;
        _employeeService = employeeService;
        _orderService = orderService;
        _statisticService = statisticService;
    }

    [HttpGet]
    public IActionResult GetInfo()
    {
        if (HttpContext.Items["Employee"] is not Employee employee)
            return StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse("internal server error"));

        var response = new EmployeeResponse(employee)
        {
            Orders = employee.Orders.Take(3).Select(o => new OrderResponse(o)).ToList()
        };

        return Ok(response);
    }

    [HttpGet]
    [Route("orders")]
    public async Task<IActionResult> GetOrders()
    {
        if (HttpContext.Items["Employee"] is not Employee employee)
            return StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse("internal server error"));

        var orders = await _orderService.GetOrdersByEmployeeIdAsync(employee.Id);

        var response = orders.Select(x => new OrderResponse(x));

        return Ok(response);
    }

    [HttpGet]
    [Route("orders/{id}")]
    public async Task<IActionResult> GetOrder(string id)
    {
        if (HttpContext.Items["Employee"] is not Employee employee)
            return StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse("internal server error"));

        var order = await _orderService.GetOrderByIdAsync(id);

        if (order.EmployeeId != employee.Id)
            return StatusCode(StatusCodes.Status403Forbidden, new ErrorResponse("forbidden"));

        var response = new OrderResponse(order)
        {
            Customer = new CustomerResponse(order.Customer),
            Configurations = order.Configurations.Select(x => new ConfigurationResponse(x))
        };

        return Ok(response);
    }

    [HttpGet]
    [Route("statistic")]
    public async Task<IActionResult> GetStatistic(DateTime from, DateTime to)
    {
        try
        {
            if (HttpContext.Items["Employee"] is not Employee employee)
                return StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse("internal server error"));

            var statistic = await _statisticService.GetForEmployeeAsync(from, to, employee.Id);

            return Ok(statistic);
        }
        catch (ArgumentException ex)
        {
            _logger.LogError(ex, "GetStatistic error");
            
            return StatusCode(StatusCodes.Status400BadRequest, new ErrorResponse(ex.Message)); 
        }
        catch (Exception e)
        {
            _logger.LogError(e, "GetStatistic error");
            
            return StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse("internal server error"));
        }
    }

}