﻿using Microsoft.AspNetCore.Mvc;
using TechTinker.Domain.Dto.Request;
using TechTinker.Domain.Dto.Response;
using TechTinker.Domain.Entities;
using TechTinker.Services.Interfaces;

namespace TechTinker.Api.Controllers;

[ApiController]
[Route("/api/employees")]
public class EmployeeController : Controller
{
    private readonly ILogger<EmployeeController> _logger;
    private readonly IEmployeeService _employeeService;
    private readonly IOrderService _orderService;
    private readonly IStatisticService _statisticService;

    public EmployeeController(IEmployeeService employeeService, ILogger<EmployeeController> logger, 
        IOrderService orderService, IStatisticService statisticService)
    {
        _logger = logger;
        _employeeService = employeeService;
        _orderService = orderService;
        _statisticService = statisticService;
    }

    [HttpGet]
    public async Task<IActionResult> GetEmployees()
    {
        try
        {
            var employees = await _employeeService.GetListAsync();
            var response = employees.Select(employee => new EmployeeResponse(employee));
            return Ok(response);
        }
        catch (ArgumentException e)
        {
            _logger.LogError(e.Message);
            
            return BadRequest(new ErrorResponse(e.Message));
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    [HttpPost]
    public async Task<IActionResult> CreateEmployee(CreateEmployeeRequest employee)
    {
        try
        {
            var createdEmployee = await _employeeService.CreateAsync(employee);
            var response = new EmployeeResponse(createdEmployee);
            return Ok(response);
        }
        catch (ArgumentException e)
        {
            _logger.LogError(e.Message);
            
            return BadRequest(new ErrorResponse(e.Message));
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
    
    [HttpGet]
    [Route("{id}")]
    public async Task<IActionResult> GetEmployeeById(string id)
    {
        try
        {
            var employee = await _employeeService.GetByIdAsync(id);
            var response = new EmployeeResponse(employee);
            return Ok(response);
        }
        catch (ArgumentException e)
        {
            _logger.LogError(e.Message);
            
            return BadRequest(new ErrorResponse(e.Message));
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    [HttpPut]
    [Route("{id}")]
    public async Task<IActionResult> UpdateEmployee(string id, UpdateEmployeeRequest body)
    {
        try
        {
            var employee = await _employeeService.UpdateAsync(id, body);
            var response = new EmployeeResponse(employee);
            return Ok(response);
        }
        catch (ArgumentException e)
        {
            _logger.LogError(e.Message);
            
            return BadRequest(new ErrorResponse(e.Message));
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    [HttpDelete]
    [Route("{id}")]
    public async Task<IActionResult> DeleteEmployee(string id)
    {
        try
        {
            if (await _employeeService.DeleteAsync(id))
                return Ok(new { id });

            return NotFound(new { id });
        }
        catch (ArgumentException e)
        {
            _logger.LogError(e.Message);
            
            return BadRequest(new ErrorResponse(e.Message));
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    [HttpPost]
    [Route("statistic")]
    public async Task<IActionResult> GetStatistic(EmployeesStatisticRequest body)
    {
        try
        {
            var statistics = await _statisticService.GetForEmployeesAsync(body.From, body.To, body.EmployeeIds);

            return Ok(statistics);
        }
        catch (ArgumentException e)
        {
            _logger.LogError(e.Message);
            
            return BadRequest(new ErrorResponse(e.Message));
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);

            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}