using System.Text.Json.Serialization;
using TechTinker.Api.Middleware;
using TechTinker.DataService.DbAccess;
using TechTinker.DataService.Repositories;
using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Services;
using TechTinker.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

{
    builder.Services.AddSingleton<DataContext>();
    builder.Services.AddCors();
    builder.Services.AddControllers().AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
        options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
    });
    
    
    builder.Services.AddTransient<IComponentRepository, ComponentRepository>();
    
    builder.Services.AddTransient<IOrderRepository, OrderRepository>();
    builder.Services.AddTransient<IOrderService, OrderService>();

    builder.Services.AddTransient<IConfigurationRepository, ConfigurationRepository>();
    builder.Services.AddTransient<IConfigurationService, ConfigurationService>();

    builder.Services.AddTransient<IEmployeeRepository, EmployeeRepository>();
    builder.Services.AddTransient<IEmployeeService, EmployeeService>();

    builder.Services.AddTransient<IStatisticService, StatisticService>();
    builder.Services.AddSingleton<IJwtService, JwtService>();

    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();
}

var app = builder.Build();

{
    if (app.Environment.IsDevelopment())
    {
        try
        {
            using var scope = app.Services.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<DataContext>();
            await context.Init();
            await context.Seed();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
}

{
    app.UseRouting();
    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseCors(options => options
        .AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader());

    if (app.Environment.IsProduction())
    {
        app.UseHttpsRedirection();
    }

    app.UseAuthorization();
    app.UseWhen(context => !context.Request.Path.StartsWithSegments("/api/auth/token"), b =>
    {
        b.UseJwtValidation();
    });
    app.UseWhen(context => context.Request.Path.StartsWithSegments("/api/employees"), 
        b =>
    {
        b.UseOnlyManagerMiddleware();
    });
    app.MapControllers();
}

app.Run();