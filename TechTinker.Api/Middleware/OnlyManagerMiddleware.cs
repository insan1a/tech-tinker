﻿using TechTinker.Domain.Dto.Response;
using TechTinker.Domain.Entities;

namespace TechTinker.Api.Middleware;

public class OnlyManagerMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<OnlyManagerMiddleware> _logger;

    public OnlyManagerMiddleware(RequestDelegate next, ILogger<OnlyManagerMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        if (context.Items["Employee"] is not Employee employee)
        {
            context.Response.StatusCode = 401;
            await context.Response.WriteAsJsonAsync(new ErrorResponse("Unauthorized"));
            return;
        }
        
        _logger.LogInformation("Employee {EmployeeId} is {Role}", employee.Id, employee.Role);

        if (employee.Role is EmployeeRole.Manager or EmployeeRole.Administrator)
        {
            await _next(context);
            return;
        }
        
        context.Response.StatusCode = 403;
        await context.Response.WriteAsJsonAsync(new ErrorResponse("Forbidden"));
    }
}

public static class OnlyManagerMiddlewareExtension
{
    public static IApplicationBuilder UseOnlyManagerMiddleware(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<OnlyManagerMiddleware>();
    }
}