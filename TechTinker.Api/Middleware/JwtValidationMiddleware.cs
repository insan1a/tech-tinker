﻿using System.Security.Claims;
using TechTinker.Domain.Dto.Response;
using TechTinker.Services.Interfaces;

namespace TechTinker.Api.Middleware;

public class JwtValidationMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<JwtValidationMiddleware> _logger;
    private readonly IJwtService _jwtService;
    private readonly IEmployeeService _employeeService;

    public JwtValidationMiddleware(RequestDelegate next, ILogger<JwtValidationMiddleware> logger, IJwtService jwtService, IEmployeeService employeeService)
    {
        _next = next;
        _logger = logger;
        _jwtService = jwtService;
        _employeeService = employeeService;
    }


    public async Task InvokeAsync(HttpContext context)
    {
        var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
        if (token is null)
        {
            context.Response.StatusCode = StatusCodes.Status401Unauthorized;

            await context.Response.WriteAsJsonAsync(new ErrorResponse("unauthorized"));
            return;
        }

        try
        {
            var principal = _jwtService.ValidateToken(token);

            var employeeId = principal.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var employee = await _employeeService.GetByIdAsync(employeeId);
            
            context.Items["Employee"] = employee;
            
            _logger.LogInformation("employee with id {employee} logged in", employeeId);

            await _next(context);
        }
        catch (Exception e)
        {
            context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            
            _logger.LogError(e, "Error validating token");
            
            await context.Response.WriteAsJsonAsync(new ErrorResponse("unauthorized"));
        }
    }
}

public static class JwtValidationMiddlewareExtension
{
    public static IApplicationBuilder UseJwtValidation(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<JwtValidationMiddleware>();
    }
}