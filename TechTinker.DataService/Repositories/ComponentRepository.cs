﻿using Dapper;
using TechTinker.DataService.DbAccess;
using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Domain.Entities;

namespace TechTinker.DataService.Repositories;

public class ComponentRepository : BaseRepository, IComponentRepository
{
    public ComponentRepository(DataContext context) : base(context)
    {
    }

    public async Task<IEnumerable<Component>> GetByIdsAsync(IEnumerable<string> ids)
    {
        using var connection = await GetConnection();
        const string sql = @"
        SELECT * FROM components WHERE id IN @ids";
        var components = await connection.QueryAsync<Component>(sql, new { ids });
        if (components == null)
            throw new ArgumentException("No components found with the given ids");
        return components;
    }
}