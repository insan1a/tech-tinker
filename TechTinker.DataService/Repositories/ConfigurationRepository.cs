﻿using Dapper;
using TechTinker.DataService.DbAccess;
using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Domain.Entities;

namespace TechTinker.DataService.Repositories;

public class ConfigurationRepository : BaseRepository, IConfigurationRepository
{
    public ConfigurationRepository(DataContext context) 
        : base(context) { }

    public async Task<IEnumerable<Configuration>> GetForOrderAsync(string orderId)
    {
        using var connection = Context.CreateConnection();
        const string sql = @"
        SELECT
            *
        FROM configurations
        WHERE orderId = @orderId
            AND deletedAt IS NULL
        ORDER BY
            created_at DESC;";
        var configurations = await connection.QueryAsync<Configuration>(sql, new {Id = orderId});
        
        const string sql2 = @"
        SELECT
            *
        FROM components
        WHERE 
            configurationId IN @configurationIds
        ORDER BY 
            configurationId;
        ";
        var componentIds = configurations.Select(x => x.Id.ToString());
        var components = await connection.QueryAsync<Component>(sql2, new {configurationIds = componentIds});
        
        foreach (var configuration in configurations)
            configuration.Components = components.Where(x => x.ConfigurationId == configuration.Id).ToList();

        return configurations;
    }

    public async Task<IEnumerable<Configuration>> GetForOrdersAsync(IEnumerable<string> orderIds)
    {
        using var connection = await GetConnection();
        const string sql = @"
        SELECT * FROM configurations
        WHERE 
            orderId IN @ids 
          AND deletedAt IS NULL
        ORDER BY orderId ASC;";
        var configurations = await connection.QueryAsync<Configuration>(sql, new {ids = orderIds});

        return configurations;
    }

    public async Task<Configuration> GetByIdAsync(string id)
    {
        using var connection = await GetConnection();

        const string sql = @"
        SELECT * FROM configurations
        WHERE id = @id AND deletedAt IS NULL;";
        var configuration =  await connection.QueryFirstOrDefaultAsync<Configuration>(sql, new {id});
        if (configuration == null)
            throw new ArgumentException($"Configuration with id {id} not found");
            
        const string sql2 = @"
        SELECT * FROM components
        WHERE configurationId = @id;";
        var components = await connection.QueryAsync<Component>(sql2, new {id});
        
        configuration.Components = components.ToList();
        return configuration;
    }

    public async Task<Configuration> AddToOrderAsync(Configuration configuration)
    {
        using var connection = Context.CreateConnection();

        const string sql = @"
                INSERT INTO configurations 
                    (id, orderId, employeeId, price)
                VALUES 
                    (@Id, @OrderId, @EmployeeId, @Price)";
        var rowAffected = await connection.ExecuteAsync(sql, configuration);

        if (rowAffected == 0)
            throw new Exception("Failed to add configuration to order");

        var ids = configuration.Components.Select(x => x.Id.ToString());
        const string sql2 = @"
                UPDATE components 
                    SET configurationId = @ConfigurationId
                WHERE id IN @ComponentIds";
        rowAffected = await connection.ExecuteAsync(sql2, new { ConfigurationId = configuration.Id, ComponentIds = ids });
            
        if (rowAffected == 0)
            throw new Exception("Failed to add components to configuration");

        return configuration;
    }

    public async Task<bool> DeleteAsync(string id)
    {
        using var connection = await GetConnection();

        const string sql = @"
        UPDATE configurations
            SET deletedAt = CURRENT_TIMESTAMP
        WHERE id = @id;";
        var row = await connection.ExecuteAsync(sql, new {id});
        if (row == 0)
            return false;

        const string sql2 = @"
        UPDATE components
            SET configurationId = NULL
        WHERE configurationId = @id;";
        await connection.ExecuteAsync(sql2, new { id });

        return true;
    }

    public async Task<Configuration> UpdateAsync(Configuration configuration)
    {
        using var connection = await GetConnection();

        const string sql = @"
        UPDATE components
            SET configurationId = NULL,
                updatedAt = CURRENT_TIMESTAMP
        WHERE
            configurationId = @id
            AND deletedAt IS NULL;";
        var row = await connection.ExecuteAsync(sql, new {id = configuration.Id});
        if (row == 0)
            throw new ArgumentException($"Failed to update configuration with id {configuration.Id}");

        const string sql2 = @"
        UPDATE components
            SET configurationId = @id,
                updatedAt = CURRENT_TIMESTAMP
        WHERE
            id IN @ids";
        await connection.ExecuteAsync(sql2, new {id = configuration.Id, ids = configuration.Components.Select(x => x.Id.ToString())});
        
        const string sql3 = @"
        UPDATE configurations
            SET price = @price,
            updatedAt = CURRENT_TIMESTAMP
        WHERE
            id = @id;";
        await connection.ExecuteAsync(sql3, new {id = configuration.Id, price = configuration.Price});

        return configuration;
    }
}