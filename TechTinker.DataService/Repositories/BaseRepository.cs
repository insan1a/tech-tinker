﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using Microsoft.Extensions.Configuration;
using TechTinker.DataService.DbAccess;

namespace TechTinker.DataService.Repositories;

public class BaseRepository
{
    protected readonly DataContext Context;

    protected BaseRepository(DataContext context)
    {
        Context = context;
    }
    
    protected Task<IDbConnection> GetConnection() =>
        Task.FromResult(Context.CreateConnection());
    
}