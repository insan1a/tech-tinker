﻿using TechTinker.Domain.Entities;

namespace TechTinker.DataService.Repositories.Interfaces;

public interface IConfigurationRepository
{
    Task<IEnumerable<Configuration>> GetForOrderAsync(string orderId);
    Task<IEnumerable<Configuration>> GetForOrdersAsync(IEnumerable<string> orderIds);
    Task<Configuration> GetByIdAsync(string id);
    Task<Configuration> AddToOrderAsync(Configuration configuration);
    Task<Configuration> UpdateAsync(Configuration configuration);
    Task<bool> DeleteAsync(string id);
}
