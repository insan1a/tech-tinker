﻿using TechTinker.Domain.Entities;

namespace TechTinker.DataService.Repositories.Interfaces;

public interface IEmployeeRepository : IGenericRepository<Employee>
{
    Task<Employee> GetByEmailAsync(string email);
    Task<IEnumerable<Order>> GetOrdersAsync(string employeeId);
    Task<Order> GetOrderAsync(string id);
}
