﻿using TechTinker.Domain.Entities;

namespace TechTinker.DataService.Repositories.Interfaces;

public interface IComponentRepository
{
    Task<IEnumerable<Component>> GetByIdsAsync(IEnumerable<string> ids);
}