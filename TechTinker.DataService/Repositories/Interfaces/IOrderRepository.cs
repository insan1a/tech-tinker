﻿using TechTinker.Domain.Entities;

namespace TechTinker.DataService.Repositories.Interfaces;

public interface IOrderRepository
{
    Task<Order> GetByIdAsync(string id);
}