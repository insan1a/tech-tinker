﻿using Dapper;
using TechTinker.DataService.DbAccess;
using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Domain.Entities;

namespace TechTinker.DataService.Repositories;

public class OrderRepository : BaseRepository, IOrderRepository
{
    public  OrderRepository(DataContext context) : base(context)
    {
    }

    public async Task<Order> GetByIdAsync(string id)
    {
        using var connection = await GetConnection();
        const string sql = @"
        SELECT * FROM orders WHERE id = @id";
        var result = await connection.QueryFirstOrDefaultAsync<Order>(sql, new { id });
        if (result == null)
            throw new ArgumentException($"Order with id {id} not found");
        return result;
    }
}