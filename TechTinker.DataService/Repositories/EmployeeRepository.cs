﻿using Dapper;
using TechTinker.DataService.DbAccess;
using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Domain.Entities;

namespace TechTinker.DataService.Repositories;

public class EmployeeRepository : BaseRepository, IEmployeeRepository
{
    public EmployeeRepository(DataContext context) 
        : base(context)
    {
    }
    public async Task<IEnumerable<Employee>> GetAllAsync()
    {
        using var connection = Context.CreateConnection();
        const string sql = @"
        SELECT 
            id,
            firstName,
            lastName,
            email,
            role,
            createdAt
        FROM employees
        WHERE deletedAt IS NULL";
        return await connection.QueryAsync<Employee>(sql);
    }

    public async Task<Employee> GetByIdAsync(string id)
    {
        using var connection = Context.CreateConnection();
            
        const string sql = @"
        SELECT 
            id, 
            firstName, 
            lastName, 
            email, 
            hashedPassword, 
            role, 
            createdAt 
        FROM employees 
        WHERE id = @id AND deletedAt IS NULL";
        var employee = await connection.QueryFirstAsync<Employee>(sql, new { id });

        if (employee == null)
            throw new KeyNotFoundException($"Employee with id {id} not found");

        const string sql2 = @"
        SELECT 
            id,
            number,
            priceLimit,
            comment,
            address,
            status,
            createdAt 
        FROM orders 
        WHERE employeeId = @id 
            AND status = @status 
            AND deletedAt IS NULL 
        ORDER BY createdAt DESC";
        var orders = await connection.QueryAsync<Order>(sql2, new { id, status = OrderStatus.InProcess });

        employee.Orders = orders.ToList();
        return employee;
    }

    public async Task<bool> AddAsync(Employee entity)
    {
        using var connection = Context.CreateConnection();
        const string sql = @"
        INSERT INTO employees 
            (id, firstName, lastName, email, hashedPassword, role) 
        VALUES 
            (@id, @firstName, @lastName, @email, @hashedPassword, @role)";
        return await connection.ExecuteAsync(sql, new
        {
            id = entity.Id,
            firstName = entity.FirstName,
            lastName = entity.LastName,
            email = entity.Email,
            hashedPassword = entity.HashedPassword,
            role = entity.Role,
        }) > 0;
    }

    public async Task<bool> UpdateAsync(Employee entity)
    {
        using var connection = Context.CreateConnection();
        const string sql = @"
        UPDATE employees 
        SET firstName = @firstName, 
            lastName = @lastName,
            role = @role, 
            updatedAt = CURRENT_TIMESTAMP 
        WHERE id = @id";
        return await connection.ExecuteAsync(sql, new
        {
            id = entity.Id,
            firstName = entity.FirstName,
            lastName = entity.LastName,
            role = entity.Role,
        }) > 0;
    }

    public async Task<bool> DeleteAsync(string id)
    {
        using var connection = Context.CreateConnection();
        const string sql = @"
        UPDATE employees 
            SET deletedAt = CURRENT_TIMESTAMP 
        WHERE id = @id";
        return await connection.ExecuteAsync(sql, new { id }) > 0;
    }

    public async Task<Employee> GetByEmailAsync(string email)
    {
        using var connection = Context.CreateConnection();
        const string sql = @"
        SELECT 
            id,
            firstName,
            lastName,
            email,
            hashedPassword,
            role,
            createdAt 
        FROM employees 
        WHERE email = @email";
        var employee = await connection.QueryFirstAsync<Employee>(sql, new{email=email});
        return employee;
    }

    public async Task<IEnumerable<Order>> GetOrdersAsync(string employeeId)
    {
        using var connection = Context.CreateConnection();
        const string sql = @"
        SELECT 
            id,
            number,
            customerId, 
            priceLimit,
            comment,
            address,
            status, 
            createdAt 
        FROM orders 
        WHERE 
            employeeId = @id 
            AND deletedAt IS NULL 
        ORDER BY 
            status ASC,
            createdAt DESC";
        return await connection.QueryAsync<Order>(sql, new { id = employeeId });
    }

    public async Task<Order> GetOrderAsync(string id)
    {
        using var connection = Context.CreateConnection();
        const string sql = @"
        SELECT 
            id,
            number,
            customerId,
            employeeId,
            priceLimit,
            comment,
            address,
            status, 
            createdAt 
        FROM orders 
        WHERE 
            id = @id 
            AND deletedAt IS NULL 
        ORDER BY 
            status ASC,
            createdAt DESC";
        var order = await connection.QueryFirstOrDefaultAsync<Order>(sql, new { id });
        if (order == null)
            throw new KeyNotFoundException($"Order with id {id} not found");

        const string sql2 = @"
        SELECT
            id,
            firstName,
            lastName,
            email,
            phoneNumber
        FROM customers
        WHERE 
            id = @id
        ";
        var customer = await connection.QueryFirstOrDefaultAsync<Customer>(sql2, new { id = order.CustomerId });
        
        order.Customer = customer;

        const string sql3 = @"
        SELECT
            id,
            price,
            createdAt
        FROM configurations
        WHERE
            orderId = @id
        ";
        var configurations = await connection.QueryAsync<Configuration>(sql3, new { id });
        
        order.Configurations = configurations.ToList();

        return order;
    }
}