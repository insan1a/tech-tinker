﻿using System.Data;
using System.Data.SQLite;
using Dapper;
using Microsoft.Extensions.Configuration;
using TechTinker.Domain.Entities;

namespace TechTinker.DataService.DbAccess;

public class DataContext
{
    private readonly IConfiguration _configuration;

    public DataContext(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public IDbConnection CreateConnection()
    {
        return new SQLiteConnection(_configuration.GetConnectionString("default"));
    }

    public async Task Init()
    {
        using var connection = CreateConnection();
        await InitTables();
        return;

        async Task InitTables()
        {
            try
            {
                const string customers = """
                CREATE TABLE IF NOT EXISTS customers (
                    id VARCHAR PRIMARY KEY NOT NULL,
                    firstName TEXT NOT NULL,
                    lastName TEXT NOT NULL,
                    email TEXT NOT NULL UNIQUE,
                    phoneNumber TEXT NOT NULL UNIQUE,
                    createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    deletedAt DATETIME
                );
                """;
                await connection.ExecuteAsync(customers);
                

                const string employees = """
                CREATE TABLE IF NOT EXISTS employees (
                    id VARCHAR PRIMARY KEY NOT NULL,
                    firstName TEXT NOT NULL,
                    lastName TEXT NOT NULL,
                    email TEXT NOT NULL UNIQUE,
                    hashedPassword TEXT NOT NULL,
                    role INTEGER NOT NULL,
                    createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    deletedAt DATETIME
                );   
                """;
                await connection.ExecuteAsync(employees);

                const string orders = @"
                CREATE TABLE IF NOT EXISTS orders (
                    id VARCHAR PRIMARY KEY NOT NULL,
                    number INTEGER,
                    customerId VARCHAR NOT NULL,
                    employeeId VARCHAR NOT NULL,
                    priceLimit INTEGER NOT NULL,
                    comment TEXT NOT NULL,
                    address TEXT NOT NULL,
                    status INTEGER NOT NULL,
                    createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    deletedAt DATETIME,
                    FOREIGN KEY(customerId) REFERENCES customers(id),
                    FOREIGN KEY(employeeId) REFERENCES employees(id)
                );
                ";
                await connection.ExecuteAsync(orders);

                const string orderNumberTrigger = @"
                CREATE TRIGGER auto_increment_trigger
    AFTER INSERT ON orders
    WHEN new.number IS NULL
BEGIN
    UPDATE orders
    SET number = (SELECT IFNULL(MAX(number), 0) + 1 FROM orders)
    WHERE id = new.id;
END;";
                await connection.ExecuteAsync(orderNumberTrigger);

                const string configurations = """
                CREATE TABLE IF NOT EXISTS configurations (
                    id VARCHAR PRIMARY KEY NOT NULL,
                    price INTEGER NOT NULL,
                    orderId VARCHAR NOT NULL,
                    employeeId VARCHAR NOT NULL,
                    createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    deletedAt DATETIME,
                    FOREIGN KEY(orderId) REFERENCES orders(id),
                    FOREIGN KEY(employeeId) REFERENCES employees(id)
                );
                """;
                await connection.ExecuteAsync(configurations);

                const string components = """
                CREATE TABLE IF NOT EXISTS components (
                    id VARCHAR PRIMARY KEY NOT NULL,
                    configurationId VARCHAR,
                    name TEXT NOT NULL,
                    description TEXT NOT NULL,
                    price INTEGER NOT NULL,
                    type INTEGER NOT NULL,
                    createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updatedAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    deletedAt DATETIME,
                    FOREIGN KEY(configurationId) REFERENCES configurations(id)
                );
                """;
                await connection.ExecuteAsync(components);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }

    public async Task Seed()
    {
        await SeedData();
        return;


        async Task SeedData()
        {
            using var connection = CreateConnection();
            // Employees
            var employees = new List<Employee>
            {
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    FirstName = "Ivan",
                    LastName = "Ivanov",
                    Email = "user@test.com",
                    HashedPassword = BCrypt.Net.BCrypt.HashPassword("pa5$wOrd"),
                    Role = EmployeeRole.Technician,
                },
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    FirstName = "Roman",
                    LastName = "Kravchuk",
                    Email = "user2@test.com",
                    HashedPassword = BCrypt.Net.BCrypt.HashPassword("pa5$wOrd"),
                    Role = EmployeeRole.Manager,
                }                
            };
            var sql = @"
            INSERT INTO employees
                (id, firstName, lastName, email, hashedPassword, role)
            VALUES
                (@Id, @FirstName, @LastName, @Email, @HashedPassword, @Role);";
            foreach (var employee in employees)
            {
                await connection.ExecuteAsync(sql, new
                {
                    employee.Id,
                    employee.FirstName,
                    employee.LastName,
                    employee.Email,
                    employee.HashedPassword,
                    employee.Role              
                });
            }

            // Customers
            var customers = new List<Customer>()
            {
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    FirstName = "Pavel",
                    LastName = "Pavlov",
                    Email = "customer@test.com",
                    PhoneNumber = "88005553535"
                }
            };
            sql = @"
            INSERT INTO customers
                (id, firstName, lastName, email, phoneNumber)
            VALUES
                (@Id, @FirstName, @LastName, @Email, @PhoneNumber);";
            foreach (var customer in customers)
            {
                await connection.ExecuteAsync(sql, new
                {
                    customer.Id,
                    customer.FirstName,
                    customer.LastName,
                    customer.Email,
                    customer.PhoneNumber,                    
                });
            }
            
            // Orders
            var orders = new List<Order>()
            {
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    CustomerId = customers[0].Id,
                    EmployeeId = employees[0].Id,
                    PriceLimit = 5000000,
                    Comment = "Test comment",
                    Address = "Test address",
                    Status = OrderStatus.InProcess,
                },
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    CustomerId = customers[0].Id,
                    EmployeeId = employees[0].Id,
                    PriceLimit = 15000000,
                    Comment = "Test comment",
                    Address = "Test address",
                    Status = OrderStatus.InProcess,
                },
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    CustomerId = customers[0].Id,
                    EmployeeId = employees[0].Id,
                    PriceLimit = 50000000,
                    Comment = "Test comment",
                    Address = "Test address",
                    Status = OrderStatus.Completed,
                }
            };
            sql = @"
            INSERT INTO orders
                (id, customerId, employeeId, priceLimit, comment, address, status)
            VALUES
                (@Id, @CustomerId, @EmployeeId, @PriceLimit, @Comment, @Address, @Status);";
            foreach (var order in orders)
            {
                await connection.ExecuteAsync(sql, new
                {
                    order.Id,
                    order.CustomerId,
                    order.EmployeeId,
                    order.PriceLimit,
                    order.Comment,
                    order.Address,
                    order.Status,
                });
            }
            
            // Configurations
            var configurations = new List<Configuration>()
            {
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    OrderId = orders[0].Id,
                    EmployeeId = employees[0].Id,
                    Price = 4500000
                },
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    OrderId = orders[0].Id,
                    EmployeeId = employees[0].Id,
                    Price = 4500000
                },
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    OrderId = orders[0].Id,
                    EmployeeId = employees[0].Id,
                    Price = 4500000
                },
            };
            sql = @"
            INSERT INTO configurations
                (id, orderId, employeeId, price)
            VALUES
                (@Id, @OrderId, @EmployeeId, @Price);";
            foreach (var configuration in configurations)
            {
                await connection.ExecuteAsync(sql, new
                {
                    configuration.Id,
                    configuration.OrderId,
                    configuration.EmployeeId,
                    configuration.Price,
                });
            }
            
            // Components
            var components = new List<Component>()
            {
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    ConfigurationId = configurations[0].Id,
                    Name = "Test component",
                    Description = "Test component description",
                    Price = 1500000,
                    Type = ComponentType.CPU
                },
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    ConfigurationId = configurations[0].Id,
                    Name = "Test component",
                    Description = "Test component description",
                    Price = 1500000,
                    Type = ComponentType.GPU
                },
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    ConfigurationId = configurations[0].Id,
                    Name = "Test component",
                    Description = "Test component description",
                    Price = 500000,
                    Type = ComponentType.RAM
                },
            };
            sql = @"
            INSERT INTO components
                (id, configurationId, name, description, price, type)
            VALUES
                (@Id, @ConfigurationId, @Name, @Description, @Price, @Type);";
            foreach (var component in components)
            {
                await connection.ExecuteAsync(sql, new
                {
                    component.Id,
                    component.ConfigurationId,
                    component.Name,
                    component.Description,
                    component.Price,
                    component.Type,
                });
            }
        }
    }
}