﻿namespace TechTinker.Domain.Dto.Response;

public class AccessTokenResponse
{
    public string Token { get; set; }

    public AccessTokenResponse(string token)
    {
        Token = token;
    }
}