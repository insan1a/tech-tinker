﻿using System.Text.Json.Serialization;
using TechTinker.Domain.Entities;

namespace TechTinker.Domain.Dto.Response;

public class OrderResponse
{
    public OrderResponse(Order order)
    {
        Id = order.Id;
        Number = order.Number;
        PriceLimit = order.PriceLimit;
        Comment = order.Comment;
        Address = order.Address;
        Status = order.Status.ToString();
        CreatedAt = order.CreatedAt;
    }

    [JsonPropertyName("id")]
    public string Id { get; set; }
    [JsonPropertyName("number")]
    public int Number { get; set; }
    [JsonPropertyName("customer")] 
    public CustomerResponse? Customer { get; set; }
    [JsonPropertyName("price_limit")]
    public int PriceLimit { get; set; }
    [JsonPropertyName("comment")]
    public string Comment { get; set; }
    [JsonPropertyName("address")]
    public string Address { get; set; }
    [JsonPropertyName("status")]
    public string Status { get; set; }
    [JsonPropertyName("created_at")]
    public DateTime CreatedAt { get; set; }
    [JsonPropertyName("configurations")]
    public IEnumerable<ConfigurationResponse> Configurations { get; set; }
}