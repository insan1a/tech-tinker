﻿using System.Text.Json.Serialization;

namespace TechTinker.Domain.Dto.Response;

public class ErrorResponse
{
    [JsonPropertyName("error")]
    public string Message { get; private set; }

    public ErrorResponse(string message)
    {
        Message = message;
    }
}