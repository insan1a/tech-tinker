﻿using System.Text.Json.Serialization;
using TechTinker.Domain.Entities;

namespace TechTinker.Domain.Dto.Response;

public class ConfigurationResponse
{
    public ConfigurationResponse(Configuration configuration)
    {
        Id = configuration.Id;
        Price = configuration.Price;
        CreatedAt = configuration.CreatedAt;
    }
    [JsonPropertyName("id")]
    public string Id { get; set; }
    [JsonPropertyName("price")] 
    public int Price { get; set; }
    [JsonPropertyName("created_at")]
    public DateTime CreatedAt { get; set; }
    [JsonPropertyName("components")]
    public IEnumerable<ComponentResponse> Components { get; set; }
}