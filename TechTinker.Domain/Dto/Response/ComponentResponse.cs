﻿using System.Text.Json.Serialization;
using TechTinker.Domain.Entities;

namespace TechTinker.Domain.Dto.Response;

public class ComponentResponse
{
    public ComponentResponse(Component component)
    {
        Id = component.Id;
        Name = component.Name;
        Description = component.Description;
        Type = component.Type.ToString();
    }

    [JsonPropertyName("id")]
    public string Id { get; set; }
    [JsonPropertyName("name")]
    public string Name { get; set; }
    [JsonPropertyName("description")]
    public string Description { get; set; }
    [JsonPropertyName("type")]
    public string Type { get; set; }
}