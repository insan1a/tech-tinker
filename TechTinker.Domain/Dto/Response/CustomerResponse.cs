﻿using System.Text.Json.Serialization;
using TechTinker.Domain.Entities;

namespace TechTinker.Domain.Dto.Response;

public class CustomerResponse
{
    public CustomerResponse(Customer customer)
    {
        Id = customer.Id;
        FirstName = customer.FirstName;
        LastName = customer.LastName;
        Email = customer.Email;
        Phone = customer.PhoneNumber;
    }

    [JsonPropertyName("id")] 
    public string Id { get; set; }
    [JsonPropertyName("first_name")] 
    public string FirstName { get; set; }
    [JsonPropertyName("last_name")] 
    public string LastName { get; set; }
    [JsonPropertyName("email")]
    public string Email { get; set; }
    [JsonPropertyName("phone")] 
    public string Phone { get; set; }
}