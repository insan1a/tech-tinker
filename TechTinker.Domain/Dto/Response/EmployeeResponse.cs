﻿using TechTinker.Domain.Entities;

namespace TechTinker.Domain.Dto.Response;

public class EmployeeResponse
{
    public EmployeeResponse(Employee employee)
    {
        Id = employee.Id;
        FirstName = employee.FirstName;
        LastName = employee.LastName;
        Email = employee.Email;
        Role = employee.Role.ToString();
    }
    public string Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Role { get; set; }
    public List<OrderResponse> Orders { get; set; }
}