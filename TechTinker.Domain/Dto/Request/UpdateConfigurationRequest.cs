﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace TechTinker.Domain.Dto.Request;

public class UpdateConfigurationRequest
{
    [Required]
    [JsonPropertyName("component_ids")]
    public required IEnumerable<string> ComponentIds { get; set; }
}