﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace TechTinker.Domain.Dto.Request;

public class EmployeesStatisticRequest
{
    [Required]
    [Timestamp]
    [JsonPropertyName("from")]
    public required DateTime From { get; set; }
    [Required]
    [Timestamp]
    [JsonPropertyName("to")]
    public required DateTime To { get; set; }
    [Required]
    [MinLength(1)]
    [MaxLength(100)]
    [JsonPropertyName("employee_ids")]
    public required IEnumerable<string> EmployeeIds { get; set; }
}