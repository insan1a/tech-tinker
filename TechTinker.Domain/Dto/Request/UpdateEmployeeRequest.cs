﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using TechTinker.Domain.Entities;

namespace TechTinker.Domain.Dto.Request;

public class UpdateEmployeeRequest
{
    [Required(ErrorMessage = "FirstName is required")]
    [StringLength(100, MinimumLength = 2, ErrorMessage = "FirstName must be between 2 and 100 characters")]
    [JsonPropertyName("first_name")]
    public required string FirstName { get; set; }

    [Required(ErrorMessage = "LastName is required")]
    [StringLength(100, MinimumLength = 2, ErrorMessage = "LastName must be between 2 and 100 characters")]
    [JsonPropertyName("last_name")]
    public required string LastName { get; set; }

    [Required(ErrorMessage = "Role is required")]
    [EnumDataType(typeof(EmployeeRole), ErrorMessage = "Role is not valid")]
    [JsonPropertyName("role")]
    public required EmployeeRole Role { get; set; }
}