﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace TechTinker.Domain.Dto.Request;

public class CreateConfigurationRequest
{
    [Required]
    [JsonPropertyName("order_id")]
    public required string OrderId { get; set; }
    [Required]
    [JsonPropertyName("component_ids")]
    public required IEnumerable<string> ComponentIds { get; set; }
}