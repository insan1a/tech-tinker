﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace TechTinker.Domain.Dto.Request;

public class Credentials
{
    [Required(ErrorMessage = "Email is required")]
    [EmailAddress(ErrorMessage = "Email is not valid")]
    [StringLength(100, MinimumLength = 5, ErrorMessage = "Email must be between 5 and 100 characters")]
    [DataType(DataType.EmailAddress)]
    [JsonPropertyName("email")]
    public required string Email { get; set; }
        
    [Required(ErrorMessage = "Password is required")]
    [StringLength(100, MinimumLength = 8, ErrorMessage = "Password must be between 8 and 100 characters")]
    [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$", ErrorMessage = "Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character")]
    [DataType(DataType.Password)]
    [JsonPropertyName("password")]
    public required string Password { get; set; }
}