﻿namespace TechTinker.Domain.Exceptions;

public class EmployeeNotFoundException : ArgumentException
{
    public EmployeeNotFoundException() : base("Employee not found")
    {}
}
