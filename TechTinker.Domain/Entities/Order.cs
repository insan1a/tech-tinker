﻿namespace TechTinker.Domain.Entities;

public class Order : BaseEntity
{
    public int Number { get; set; }
    public int PriceLimit { get; set; }
    public string Comment { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public OrderStatus Status { get; set; }

    public string? CustomerId { get; set; }
    public virtual Customer Customer { get; set; }

    public string? EmployeeId { get; set; }
    public virtual Employee? Employee { get; set; }
    
    public virtual ICollection<Configuration> Configurations { get; set; } = new HashSet<Configuration>();
}

public enum OrderStatus
{
    InProcess,
    Completed
}
