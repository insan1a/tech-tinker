﻿using System.ComponentModel;
using System.Text.Json.Serialization;
using TechTinker.Domain.Dto.Response;

namespace TechTinker.Domain.Entities;

public class Statistic
{
    public EmployeeResponse Employee { get; set; }
    public required Period Period { get; set; }
    public required int Total { get; set; }
    public required IEnumerable<Budget> Budgets { get; set; }

    public override bool Equals(object? obj)
    {
        return base.Equals(obj);
    }

    protected bool Equals(Statistic other)
    {
        return Employee.Equals(other.Employee) && Period.Equals(other.Period) && Total == other.Total && Budgets.Equals(other.Budgets);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Employee, Period, Total, Budgets);
    }
}

public class Period
{
    public Period(DateTime start, DateTime end)
        => (Start, End) = (start, end);
    public DateTime Start { get; set; }
    public DateTime End { get; set; }
}

public class Budget
{
    public Budget(BudgetType type, int count)
        => (BudgetType, Type, Count) = (type, type.ToDescription(), count);
    [JsonIgnore]
    public BudgetType BudgetType { get; set; }
    public string Type { get; set; }
    public int Count { get; set; }
}

public enum BudgetType
{
    LowerThan50K,
    Between50KAnd100K,
    Between100KAnd200K, 
    HigherThan200K
}

public static class MyEnumExtensions
{
    public static string ToDescription(this BudgetType type)
    {
        return type switch
        {
            BudgetType.LowerThan50K => "Less than 50K",
            BudgetType.Between50KAnd100K => "Between 50K and 100K",
            BudgetType.Between100KAnd200K => "Between 100K and 200K",
            BudgetType.HigherThan200K => "Over 200K",
            _ => string.Empty
        };
    }
}