﻿namespace TechTinker.Domain.Entities;

public class Claims
{
    public string EmployeeId { get; set; }
    public EmployeeRole Role { get; set; }

    public Claims(string employeeId, EmployeeRole role)
    {
        EmployeeId = employeeId;
        Role = role;
    }
}