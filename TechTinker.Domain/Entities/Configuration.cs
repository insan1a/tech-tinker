﻿namespace TechTinker.Domain.Entities;

public class Configuration : BaseEntity
{
    public int Price { get; set; }

    public string? OrderId { get; set; }
    public virtual Order? Order { get; set; }

    public string? EmployeeId { get; set; }
    public virtual Employee? Employee { get; set; }

    public virtual ICollection<Component> Components { get; set; } = new HashSet<Component>();
}