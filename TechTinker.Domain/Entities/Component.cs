﻿namespace TechTinker.Domain.Entities;

public class Component : BaseEntity
{
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int Price { get; set; }
    public ComponentType Type { get; set; }

    public string? ConfigurationId { get; set; }
    public virtual Configuration? Configuration { get; set; }
}

public enum ComponentType
{
    Motherboard,
    CPU,
    RAM,
    GPU,
    Storage,
    PowerSupply,
    Case,
    Cooling,
    Fan,
    CaseFan,
}