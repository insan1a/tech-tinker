﻿using TechTinker.Domain.Dto.Request;

namespace TechTinker.Domain.Entities;

public class Employee : BaseEntity
{
    public Employee(string firstName, string lastName, string email, string hashedPassword, EmployeeRole role)
    {
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        HashedPassword = BCrypt.Net.BCrypt.HashPassword(hashedPassword);
        Role = role;
    }

    public Employee(string id, UpdateEmployeeRequest request)
    {
        Id = id;
        FirstName = request.FirstName;
        LastName = request.LastName;
        Role = request.Role;
    }

    public Employee()
    {
    }

    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public string HashedPassword { get; set; } = string.Empty;

    public EmployeeRole Role { get; set; }

    public virtual ICollection<Order> Orders { get; set; } = new HashSet<Order>();
    public virtual ICollection<Configuration> Configurations { get; set; } = new HashSet<Configuration>();

    public bool VerifyPassword(string password)
    {
        return BCrypt.Net.BCrypt.Verify(password, HashedPassword);
    }
}

public enum EmployeeRole
{
    Manager,
    Technician,
    Administrator
}   
