# Tech Tinker 

## Предметная область

Заказ содержит в себе информацию о клиенте, денежном лимите, пожелании клиента, адрес доставки, статус, конфигурации и сборщике, закрепленным за ним.

Сборщик просматривает заказы которые за ним закреплены, в каждый заказ он может добавить конфигурацию, которая не превышает лимит заказа. Конфигураций в заказе может быть максимум 3. В самой конфигурации должны быть комплектующие каждого типа хотя бы 1 шт. В разных конфигурациях не могут находится одни и те же комплектующие. Также сотрудник может просмотреть личный кабинет и статистику по выполненым заказам за определенный месяц в пределах одного года.

Менеджер может добавлять/редактировать/удалять сборщиков, закреплять заказы за ними и помечать заказы как выполненные. Также он имеет доступ к статистике по каждому сборщику за определенный месяц в пределах одного года.

## Схема Базы данных

![database-schema](./Docs/Assets/database-schema.jpg)

## API

### `POST /api/auth/token`

Получение токена доступа.

#### Request

```json
{
  "email": "ivan.ivanov@techtinker.com",
  "password": "pa5$wOrd"
}
```

#### Response

```json
{
  "token": "string"
}
```

### `GET /api/current`

Получение информации текущего сотрудника

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

#### Response

```json
{
  "id": "string",
  "first_name": "string",
  "last_name": "string",
  "email": "string",
  "role": "string",
  "orders": []
}
```

### `GET /api/current/orders`

Получение заказов текущего сотрудника

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

#### Response

```json
[
  {
    "id": "string",
    "customer": {
      "id": "string",
      "first_name": "string",
      "last_name": "string",
      "email": "string",
      "phone": "string"
    },
    "price_limit": 0,
    "comment": "string",
    "address": "string",
    "status": "string",
    "created_at": "string"
  }
]
```

### `GET /api/current/orders/{id}`

Получение заказов текущего сотрудника

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

#### Response

```json
{
  "id": "string",
  "customer": {
    "id": "string",
    "first_name": "string",
    "last_name": "string",
    "email": "string",
    "phone": "string"
  },
  "price_limit": 0,
  "comment": "string",
  "address": "string",
  "status": "string",
  "created_at": "string",
  "configurations": [
    {
      "id": "string",
      "price": 0,
      "created_at": "string"
    }
  ]
}
```

### `GET /api/current/statistic`

Получение статистики по заказам текущего сотрудника

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

| Query param | value       |
|-------------|-------------|
| from        | 2021-01-01  | 
| to          | 2021-12-01  |

#### Response

```json
{
    "period": {
        "start": "2023-12-01T00:00:00",
        "end": "2023-12-31T00:00:00"
    },
    "total": 1000000,
    "budgets": [
        {
            "type": "Over 200K",
            "count": 1
        }
    ]
}
```

### `POST /api/configurations`

Создать новую конфигурацию

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

```json
{
  "order_id": "string",
  "components": [
    "string",
    "string"
  ]
}
```

#### Response

```json
{
  "id": "string",
  "order_id": "string",
  "employee_id": "string",
  "price": 0,
  "components": [
    {
      "id": "string",
      "name": "string",
      "description": "string",
      "price": 0,
      "type": "string"
    }
  ]
}
```

### `GET /api/configurations/{id}`

Получение конфигурации

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token |

#### Response

```json
{
  "id": "string",
  "order_id": "string",
  "employee_id": "string",
  "price": 0,
  "components": [
    {
      "id": "string",
      "name": "string",
      "description": "string",
      "price": 0,
      "type": "string"
    }
  ]
}
```

### `PUT /api/configurations/{id}`

Изменение конфигурации

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

```json
{
  "order_id": "string",
  "components": [
    "string",
    "string"
  ]
}
```

#### Response

```json
{
  "id": "string",
  "order_id": "string",
  "employee_id": "string",
  "price": 0,
  "components": [
    {
      "id": "string",
      "name": "string",
      "description": "string",
      "price": 0,
      "type": "string"
    }
  ]
}
```

### `DELETE /api/configurations/{id}`

Удаление конфигурации

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

### `GET /api/employees`

Список сотрудников

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

#### Response

```json
[
	{
		"id": "5820cab4-be1e-4ae8-9064-6d53ad73bfc8",
		"firstName": "Ivan",
		"lastName": "Ivanov",
		"email": "ivan.ivanov@techtinker.com",
		"role": "Technician"
	}
]
```

### `POST /api/employees`

Создание сотрудника

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

```json
{
	"first_name":"Leha",
	"last_name":"Aloha",
	"email":"leha@techtinker.com",
	"password":"pa5$wOrd",
	"role":"Technician"
}
```

#### Response

```json
{
	"id": "fe7a8213-0773-4afd-8790-3f80dbbcd870",
	"firstName": "Leha",
	"lastName": "Aloha",
	"email": "leha@techtinker.com",
	"role": "Technician"
}
```

### `GET /api/employees/{id}`

Получение сотрудника по ID

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

#### Response

```json
{
	"id": "ecaf3397-1e65-4995-9868-7e647cb73d0c",
	"firstName": "Ivan",
	"lastName": "Ivanov",
	"email": "user@test.com",
	"role": "Technician"
}
```

### `PUT /api/employees/{id}`

Обновление сотрудника

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

```json
{
	"first_name":"Roman",
	"last_name":"Romanov",
	"role":"Technician"
}
```

#### Response

```json
{
	"id": "ecaf3397-1e65-4995-9868-7e647cb73d0c",
	"firstName": "Ivan",
	"lastName": "Ivanov",
	"email": "user@test.com",
	"role": "Technician"
}
```

### `DELETE /api/employees/{id}`

Удаление сотрудника

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 

#### Response

```json
{
  "id": "string"
}
```

### `GET /api/employees/statistic`

Статистика по сотрудникам

#### Request

| Header        | value        |
|---------------|--------------|
| Authorization | Bearer token | 


```json
{
  "from": "2023-12-01",
  "to": "2023-12-31",
  "employee_ids": [
   "ecaf3397-1e65-4995-9868-7e647cb73d0c"
  ]
}
```

#### Response

```json
[
	{
		"employee": {
			"id": "ecaf3397-1e65-4995-9868-7e647cb73d0c",
			"firstName": "Ivan",
			"lastName": "Ivanov",
			"email": "user@test.com",
			"role": "Technician"
		},
		"period": {
			"start": "2023-12-01T00:00:00",
			"end": "2023-12-31T00:00:00"
		},
		"total": 1000000,
		"budgets": [
			{
				"type": "Over 200K",
				"count": 1
			}
		]
	}
]
```