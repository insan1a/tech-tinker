﻿using NUnit.Framework;
using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Domain.Entities;
using TechTinker.Services;
using Telerik.JustMock;

namespace TestProject.Tests.Services;

[TestFixture]
public class StatisticServiceTests
{
    [Test]
    public void GetForEmployeeAsync_ShouldReturn()
    {
        var mockEmployeeRepository = Mock.Create<IEmployeeRepository>();
        var mockConfigurationRepository = Mock.Create<IConfigurationRepository>();
        var service = new StatisticService(mockEmployeeRepository, mockConfigurationRepository);
        
        var date = DateTime.Now;
        var id = Guid.NewGuid().ToString();
        var ordersIds = new  List<string>()
        {
            Guid.NewGuid().ToString(),
            Guid.NewGuid().ToString(),
        };

        var expectedStatistic = new Statistic
        {
            Period = new Period(date, date.AddDays(31)),
            Total = 600000,
            Budgets = new []
            {
                new Budget(BudgetType.Between100KAnd200K, 1),
                new Budget(BudgetType.HigherThan200K, 1),
            }
        };
        
        Mock.Arrange(() => mockEmployeeRepository.GetOrdersAsync(id))
            .Returns(Task.FromResult((IEnumerable<Order>)new List<Order>
            {
                new()
                {
                    Id = ordersIds[0],
                    EmployeeId = id,
                    PriceLimit = 100000,
                    Status = OrderStatus.Completed,
                    UpdatedAt = date.AddDays(5),
                },
                new()
                {
                    Id = ordersIds[1],
                    EmployeeId = id,
                    PriceLimit = 500000,
                    Status = OrderStatus.Completed,
                    UpdatedAt = date.AddDays(5),
                },
            }));

        Mock.Arrange(() => mockConfigurationRepository.GetForOrdersAsync(ordersIds))
            .Returns(Task.FromResult((IEnumerable<Configuration>)new List<Configuration>()
            {
                new()
                {
                    OrderId = ordersIds[0],
                    Price = 100000,
                },
                new()
                {
                    OrderId = ordersIds[1],
                    Price = 500000,
                }
            }));
        var result = service.GetForEmployeeAsync(date, date.AddDays(31), id).Result;
        
        Assert.That(result, Is.Not.Null);
        Assert.Multiple(() =>
        {
            Assert.That(result.Total, Is.EqualTo(expectedStatistic.Total));
            Assert.That(result.Budgets.Any(x => x is { BudgetType: BudgetType.HigherThan200K, Count: 1 }), Is.True);
        });
        Assert.Multiple(() =>
        {
            Assert.That(result.Budgets.Any(x => x is { BudgetType: BudgetType.Between100KAnd200K, Count: 1 }), Is.True);
            Assert.That(result.Period.Start, Is.EqualTo(expectedStatistic.Period.Start));
            Assert.That(result.Period.End, Is.EqualTo(expectedStatistic.Period.End));
        });
    }

    [Test]
    public void GetForEmployeeAsync_ShouldThrowWhenFromDateGreatThanToDate()
    {
        var service = new StatisticService(null!, null!);
        Assert.ThrowsAsync<ArgumentException>(() => service.GetForEmployeeAsync(DateTime.Now, DateTime.Now.AddDays(-1), ""));
    }
    
    [Test]
    public void GetForEmployeeAsync_ShouldThrowWhenDaysBetweenDatesGreatThan31()
    {
        var service = new StatisticService(null!,null!);
        Assert.ThrowsAsync<ArgumentException>(() => service.GetForEmployeeAsync(DateTime.Now, DateTime.Now.AddDays(32), ""));
    }

    [Test]
    public void GetForEmployeesAsync()
    {
        var mockEmployeeRepository = Mock.Create<IEmployeeRepository>();
        var mockConfigurationRepository = Mock.Create<IConfigurationRepository>();
        var service = new StatisticService(mockEmployeeRepository, mockConfigurationRepository);
        
        var date = DateTime.Now;
        var employeeIds = new List<string>
        {
            Guid.NewGuid().ToString(),
            Guid.NewGuid().ToString(),
        };

        var ordersIds = new List<string>
        {
            Guid.NewGuid().ToString(),
            Guid.NewGuid().ToString(),
            Guid.NewGuid().ToString(),
            Guid.NewGuid().ToString(),
        };

        Mock.Arrange(() => mockEmployeeRepository.GetByIdAsync(employeeIds[0]))
            .Returns(Task.FromResult(new Employee
            {
                Id = employeeIds[0],
            }));

        Mock.Arrange(() => mockEmployeeRepository.GetByIdAsync(employeeIds[1]))
            .Returns(Task.FromResult(new Employee
            {
                Id = employeeIds[1],
            }));
        
        Mock.Arrange(() => mockEmployeeRepository.GetOrdersAsync(employeeIds[0]))
            .Returns(Task.FromResult((IEnumerable<Order>)new List<Order>
            {
                new()
                {
                    Id = ordersIds[0],
                    EmployeeId = employeeIds[0],
                    PriceLimit = 25000,
                    Status = OrderStatus.Completed,
                    UpdatedAt = date.AddDays(5),
                },
                new()
                {
                    Id = ordersIds[1],
                    EmployeeId = employeeIds[0],
                    PriceLimit = 55000,
                    Status = OrderStatus.Completed,
                    UpdatedAt = date.AddDays(5),
                },
            }));


        Mock.Arrange(() => mockEmployeeRepository.GetOrdersAsync(employeeIds[1]))
            .Returns(Task.FromResult((IEnumerable<Order>)new List<Order>
            {
                new()
                {
                    Id = ordersIds[2],
                    EmployeeId = employeeIds[1],
                    PriceLimit = 100000,
                    Status = OrderStatus.Completed,
                    UpdatedAt = date.AddDays(5),
                },
                new()
                {
                    Id = ordersIds[3],
                    EmployeeId = employeeIds[1],
                    PriceLimit = 500000,
                    Status = OrderStatus.Completed,
                    UpdatedAt = date.AddDays(5),
                },
            }));

        Mock.Arrange(() => mockConfigurationRepository.GetForOrdersAsync(ordersIds.GetRange(0, 2)))
            .Returns(Task.FromResult((IEnumerable<Configuration>)new List<Configuration>()
            {
                new()
                {
                    OrderId = ordersIds[0],
                    Price = 25000,
                },
                new()
                {
                    OrderId = ordersIds[1],
                    Price = 55000,
                },
            }));

        Mock.Arrange(() => mockConfigurationRepository.GetForOrdersAsync(ordersIds.GetRange(2, 2)))
            .Returns(Task.FromResult((IEnumerable<Configuration>)new List<Configuration>()
            {
                new()
                {
                    OrderId = ordersIds[2],
                    Price = 100000,
                },
                new()
                {
                    OrderId = ordersIds[3],
                    Price = 500000,
                }
            }));
        
        var result = service.GetForEmployeesAsync(date, date.AddDays(31), employeeIds).Result.ToList();
        Assert.That(result, Is.Not.Null);
        Assert.That(result, Has.Count.EqualTo(2));
        Assert.Multiple(() =>
        {
            Assert.That(result.First().Total, Is.EqualTo(80000));
            Assert.That(result.First().Employee.Id, Is.EqualTo(employeeIds[0]));
            Assert.That(result.Last().Total, Is.EqualTo(600000));
            Assert.That(result.Last().Employee.Id, Is.EqualTo(employeeIds[1]));
        });
    }
}