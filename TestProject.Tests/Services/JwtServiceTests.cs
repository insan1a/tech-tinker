﻿using System.Security.Claims;
using NUnit.Framework;
using TechTinker.Domain.Entities;
using TechTinker.Services;
using TechTinker.Services.Interfaces;

namespace TestProject.Tests.Services;

[TestFixture]
public class JwtServiceTests
{
    private IJwtService _service = null!;

    [SetUp]
    public void Setup()
    {
        _service = new JwtService();
    }
    
    [Test]
    [TestCase(EmployeeRole.Technician)]
    [TestCase(EmployeeRole.Manager)]
    [TestCase(EmployeeRole.Administrator)]
    public void GenerateToken(EmployeeRole role)
    {
        var claims = new Claims(Guid.NewGuid().ToString(), role);
        var result = _service.GenerateToken(claims);
        
        Assert.That(result, Is.Not.Null);
        Assert.That(result, Is.Not.Empty);
    }


    [Test]
    [TestCase(EmployeeRole.Technician)]
    [TestCase(EmployeeRole.Manager)]
    [TestCase(EmployeeRole.Administrator)]
    public void ValidateToken(EmployeeRole role)
    {
        var id = Guid.NewGuid().ToString();
        var claims = new Claims(id, role);
        var token = _service.GenerateToken(claims);
        
        var result = _service.ValidateToken(token);

        Assert.That(result, Is.Not.Null);
        Assert.That(result.Claims, Is.Not.Null);
        Assert.That(result.Claims, Is.Not.Empty);
        Assert.Multiple(() =>
        {
            Assert.That(result.Claims.Any(x => x.Type == ClaimTypes.NameIdentifier), Is.True);
            Assert.That(result.Claims.Any(x => x.Type == ClaimTypes.Role), Is.True);
            Assert.That(result.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value, Is.EqualTo(id));
            Assert.That(result.Claims.First(x => x.Type == ClaimTypes.Role).Value, Is.EqualTo(role.ToString()));
        });
    }
}