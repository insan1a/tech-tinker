﻿using NUnit.Framework;
using TechTinker.DataService.Repositories.Interfaces;
using TechTinker.Domain.Entities;
using TechTinker.Services;
using Telerik.JustMock;
using Telerik.JustMock.Helpers;

namespace TestProject.Tests.Services;

[TestFixture]
public class OrderServiceTests
{
    [Test]
    public void GetOrdersByEmployeeIdAsync_ShouldReturnList()
    {
        // Arrange
        var id = Guid.NewGuid().ToString();
        var mockEmployeeRepository = Mock.Create<IEmployeeRepository>();

        Mock.Arrange(() => mockEmployeeRepository.GetOrdersAsync(id)).
            Returns(Task.FromResult((IEnumerable<Order>)new List<Order>
            {
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    EmployeeId = id
                },
                new()
                {
                    Id = Guid.NewGuid().ToString(),
                    EmployeeId = id
                }
            }));
        var service = new OrderService(mockEmployeeRepository);
        
        // Act
        var result = service.GetOrdersByEmployeeIdAsync(id).Result;
        
        // Assert
        Assert.That(result, Is.Not.Null);
        Assert.That(result, Is.Not.Empty);
        Assert.Multiple(() =>
        {
            Assert.That(result.Count(), Is.EqualTo(2));
            Assert.That(result.First().EmployeeId, Is.EqualTo(id));
            Assert.That(result.Last().EmployeeId, Is.EqualTo(id));
        });
    }


    [Test]
    public void GetOrderByIdAsync_ShouldReturn()
    {
        // Arrange
        var id = Guid.NewGuid().ToString();
        var mockEmployeeRepository = Mock.Create<IEmployeeRepository>();
        Mock.Arrange(() => mockEmployeeRepository.GetOrderAsync(id)).Returns(Task.FromResult(new Order
        {
            Id = id,
            EmployeeId = Guid.NewGuid().ToString()
        }));
        var service = new OrderService(mockEmployeeRepository);
        
        // Act
        var result = service.GetOrderByIdAsync(id).Result;
        
        // Assert
        Assert.That(result, Is.Not.Null);
        Assert.Multiple(() =>
        {
            Assert.That(result.Id, Is.EqualTo(id));
            Assert.That(result.EmployeeId, Is.Not.Null);
        });
    }
}